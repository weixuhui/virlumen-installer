<?php

namespace App\Logic;

use App\Traits\QuickVerify;

class BaseLogic
{
    use QuickVerify;
}
