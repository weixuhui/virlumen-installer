<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait Migrate
{
    /**
     * 数据表名称 不带表前缀
     * @var string
     */
    protected $table = '';

    /**
     * 添加数据表注释
     * @param string $tableComment 表注释
     */
    public function tableComment(string $tableComment=''){
        if($this->table) {
            Db::statement("ALTER TABLE `" . env('DB_PREFIX', '') . "{$this->table}` comment '{$tableComment}'");
        }
    }
}
