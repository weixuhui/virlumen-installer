<?php

namespace App\Traits;

use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Support\Facades\Validator;

/**
 * 快速验证
 * Trait QuickVerify
 * @package App\Traits
 */
trait QuickVerify
{
    /**
     * @param array $data
     * @param array $rules
     * @param array $message
     * @param array $customAttributes
     */
    public static function quickVerify(array $data = [], array $rules = [], array $message = [], array $customAttributes = [])
    {
        $validator = Validator::make($data, $rules, $message, $customAttributes);
        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors());
        }
    }
}
