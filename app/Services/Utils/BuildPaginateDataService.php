<?php

namespace App\Services\Utils;

use Dingo\Api\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class BuildPaginateDataService
{
    /**
     * LengthAwarePaginator 分页类转化
     * @param $paginate
     * @return array
     */
    public static function build($paginate)
    {
        if ($paginate instanceof LengthAwarePaginator) {
            $responseData = [
                'list' => $paginate->items(),
                'pagination' => [
                    'current' => Request::page(),
                    'pageSize' => Request::pageSize(),
                    'page_total' => $paginate->lastPage(),
                    'total' => $paginate->total()
                ]
            ];
            return $responseData;
        } else {
            return $paginate;
        }
    }
}
